import {parse, stringify} from "yaml";
import fs from "fs";
import {compile} from "../translator_3.0.js";
import {execute} from "../processor.js";

export function do_test(test_name) {
    let test_content = fs.readFileSync("golden/tests/" + test_name + ".yaml").toString()

    let test_info = parse(test_content)

    let expected_output = test_info.expected_output
    let program_input = [...test_info.input]

    let output = []
    let program_opers = JSON.parse(fs.readFileSync("buildin_lib.json").toString())

    let mem = compile(test_info.name, test_info.source, program_opers)

    let logs = execute(program_opers, mem, program_input, output)

    if(test_info.needs_logs){
        test_info.output = output
        test_info.logs = logs
        fs.writeFileSync("golden/tests/" + test_info.name + ".yaml", stringify(test_info))
    }

    if(!expected_output.every((ch, i) => (ch + "") === (output[i] + ""))){
        throw `test [${test_info.name}] failed!`
    }
    console.log(`test [${test_info.name}] OK`)
}
