import fs from "fs";

const opcode = {
    none: "none",

    push: "push",  // a b -> ...a b v
    load: "load",
    save: "save", // b val -> ...b | mem[addr] = val
    pop: "pop",

    call: "call", // call stack: [ret_addr]
    ret: "ret",  // jmp (call stack).pop

    jmp: "jmp",
    jnt: "jnt", // delete the condition result on the stack
    read: "read", // ...a b -> ...a b c (c is from input)
    write: "write", // ...a b -> b goes to output

    hlt: "hlt",

    input: "input",
    output: "output",

    binary_raw: "binary",
    binary: {
        "%": "mod",
        "+": "pls",
        "/": "frc",
        ">": "grt",
        "<": "lth",
        "==": "eql",
        "-": "mns",
        "||": "or", // выше гор
        "&&": "and"
    }
}

class ProgramMemory {
    constructor(mem) {
        this.mem = mem
        this.address = 0
        this.input_buffer = []
        this.output_buffer = []
    }

    latch_address(val) {
        if (val < 0 || val >= this.mem.length) throw "out of memory bounds!"
        this.address = val
    }

    load() {
        return this.mem[this.address]
    }

    save(val) {
        this.mem[this.address] = val
    }
}

class InstructionMemory {
    constructor(mem) {
        this.mem = mem
    }

    get_instruction(pc) {
        if (pc < 0 || pc >= this.mem.length) throw "out of memory bounds!"
        return this.mem[pc]
    }
}
class IO_controller {
    constructor(output, input) {
        this.output = output
        this.input = input
    }

}
class DataPath {


    constructor(stack_size, output, input) {

        this.output = output
        this.input = input
        this.io_controller = new IO_controller(output, input)

        this.data_stack = []
        this.call_back_stack = []

        this.data_s_head = 0
        this.call_s_head = 0

        this.stack_size = stack_size

        this.latch_head_allowed = new Set([-1, -3, 1, -2])

        this.alu = 0
    }

    get_tos_data_s(offset) {
        if (offset < 0 || offset > 3) throw "You have access only to the first 3 elements from top"
        if (offset > this.data_s_head) throw "Trying to get non-existent element"

        return this.data_stack[this.data_s_head - 1 - offset]
    }

    get_tos_call_s(offset) {
        if (offset < 0 || offset > 3) throw "You have access only to the first 3 elements from top"
        if (offset > this.call_s_head) throw "Trying to get non-existent element"

        return this.call_back_stack[this.call_s_head - 1 - offset]
    }

    latch_head_data_s(latch_val) {
        if (!this.latch_head_allowed.has(latch_val)) throw "invalid latch value"
        this.data_s_head += latch_val

        if (this.data_s_head < 0 || this.data_s_head >= this.stack_size) throw "out of memory for data stack"
    }

    latch_head_call_s(latch_val) {
        if (!this.latch_head_allowed.has(latch_val)) throw "invalid latch value"
        this.call_s_head += latch_val

        if (this.call_s_head < 0 || this.call_s_head >= this.stack_size) throw "out of memory for call stack"

    }

    push_data_s(val) {
        this.data_stack[this.data_s_head] = val
        this.latch_head_data_s(1)
    }

    push_call_s(val) {
        this.call_back_stack[this.call_s_head] = val
        this.latch_head_call_s(1)
    }

    pop_data_s() {
        this.data_stack[this.call_s_head] = 0
        this.latch_head_data_s(-1)
    }

    pop_call_s() {
        this.call_back_stack[this.call_s_head] = 0
        this.latch_head_call_s(-1)
    }

    is_true() {
        let res = !!this.get_tos_data_s(0)
        this.latch_head_data_s(-1)
        return res
    }

    latch_alu(oper_code) {

        switch (oper_code) {
            case opcode.binary['+']:
                this.alu = +this.get_tos_data_s(1) + +this.get_tos_data_s(0);
                break
            case opcode.binary['%']:
                this.alu = +this.get_tos_data_s(1) % +this.get_tos_data_s(0)
                break
            case opcode.binary['/']:
                this.alu = +this.get_tos_data_s(1) / +this.get_tos_data_s(0)
                break
            case opcode.binary['<']:
                this.alu = +this.get_tos_data_s(1) < +this.get_tos_data_s(0)
                break
            case opcode.binary['>']:
                this.alu = +this.get_tos_data_s(1) > +this.get_tos_data_s(0)
                break
            case opcode.binary['==']:
                this.alu = +this.get_tos_data_s(1) === +this.get_tos_data_s(0)
                break
            case opcode.binary['-']:
                this.alu = +this.get_tos_data_s(1) - +this.get_tos_data_s(0)
                break
            case opcode.binary['||']:
                this.alu = +this.get_tos_data_s(1) || +this.get_tos_data_s(0)
                break
            case opcode.binary['&&']:
                this.alu = +this.get_tos_data_s(1) && +this.get_tos_data_s(0)
                break
        }
    }

}

class ControlUnit {

    constructor(data_path, prog_mem, instr_mem) {
        this.data_path = data_path
        this.prog_mem = prog_mem
        this.instr_mem = instr_mem
        this.tick = 0
        this.pc = 0
        this.curr_instr = null
        this.running = true

        this.prog_mem[2] = !(this.data_path.input && this.data_path.input.length > 0);
    }

    latch_pc(next_oper) {
        if (next_oper) {
            this.pc++
        } else {
            this.pc = +this.curr_instr.param
        }
    }

    do_tick() {
        this.tick++;
    }

    cur_tick() {
        return this.tick;
    }

    decode_and_execute() {
        this.curr_instr = this.instr_mem.get_instruction(this.pc)

        if (this.curr_instr.opcode === opcode.hlt) {
            this.running = false
        }

        if (this.curr_instr.opcode === opcode.push) {
            this.data_path.push_data_s(this.curr_instr.param)
            this.latch_pc(true)
            this.do_tick()
        }

        if (this.curr_instr.opcode === opcode.load) {
            this.prog_mem.latch_address(this.curr_instr.param)
            let val = this.prog_mem.load()
            this.data_path.push_data_s(val)
            this.latch_pc(true)
            this.do_tick()
        }

        if (this.curr_instr.opcode === opcode.pop) {
            this.data_path.pop_data_s()
            this.latch_pc(true)
            this.do_tick()
        }

        if (this.curr_instr.opcode === opcode.jmp) {
            this.latch_pc(false)
            this.do_tick()
        }

        if (this.curr_instr.opcode === opcode.jnt) {
            let cond_met = this.data_path.is_true()

            if (cond_met) {
                this.latch_pc(true)
            } else {
                this.latch_pc(false)
            }
            this.do_tick()
        }

        if (this.curr_instr.opcode === opcode.binary_raw) {
            this.data_path.latch_alu(this.curr_instr.param)
            this.do_tick()
            this.data_path.latch_head_data_s(-2)
            this.do_tick()
            this.data_path.push_data_s(this.data_path.alu)
            this.latch_pc(true)
            this.do_tick()
        }

        if (this.curr_instr.opcode === opcode.none) {
            this.latch_pc(true)
            this.do_tick()
        }

        if (this.curr_instr.opcode === opcode.save) {
            this.prog_mem.latch_address(this.curr_instr.param)
            let val_to_save = this.data_path.get_tos_data_s(0)
            this.prog_mem.save(val_to_save)
            this.latch_pc(true)
            this.do_tick()
        }

        if (this.curr_instr.opcode === opcode.call) {
            this.data_path.push_call_s(this.pc + 1)
            this.latch_pc(false)
            this.do_tick()

        }

        if (this.curr_instr.opcode === opcode.ret) {
            let addr_ret_to = this.data_path.get_tos_call_s(0)
            this.pc = addr_ret_to
            this.data_path.pop_call_s()
            this.do_tick()

        }

        if (this.curr_instr.opcode === opcode.input) {
            let addr = this.data_path.get_tos_data_s(0)
            this.prog_mem.latch_address(addr)
            this.do_tick()

            this.prog_mem.save(this.data_path.io_controller.input.shift())
            if (this.data_path.input.length < 1) this.prog_mem.mem[2] = true
            this.do_tick()

            this.latch_pc(true)
            this.do_tick()
        }

        if (this.curr_instr.opcode === opcode.output) {
            let addr = this.data_path.get_tos_data_s(0)
            this.prog_mem.latch_address(addr)
            this.do_tick()

            let val_to_output = this.prog_mem.load()
            if(val_to_output) this.data_path.io_controller.output.push(val_to_output)
            this.do_tick()

            this.latch_pc(true)
            this.do_tick()

        }
    }
}

function simulate(operations, prog_data, output, input) {
    const stack_size = 1000
    let logs = []

    let prog_data_mem = new ProgramMemory(prog_data)
    let instr_mem = new InstructionMemory(operations)
    let data_path = new DataPath(stack_size, output, input)
    let control_unit = new ControlUnit(data_path, prog_data_mem, instr_mem)

    try {
        while (control_unit.running) {
            control_unit.decode_and_execute()
            logs.push({
                pc: control_unit.pc,
                tick: control_unit.tick,
                instr: control_unit.curr_instr.opcode,
                line: control_unit.curr_instr.term
            })
        }
    } catch (e) {
        console.log(e)
    }
    return logs
}

export function execute(commands, prog_mem, inp, out) {
    return simulate(commands, prog_mem, out, inp)
}