const opcode = {
    none: "none",

    push: "push",  // a b -> ...a b v
    load: "load", // a b -> a b mem[param]
    save: "save", // b val -> ...b | mem[addr] = val
    pop: "pop",

    call: "call", // call stack: [ret_addr]
    ret: "ret",  // jmp (call stack).pop

    jmp: "jmp",
    jnt: "jnt", // delete the condition result on the stack

    read: "read", // ...a b -> ...a b c (c is from input)
    write: "write", // ...a b -> b goes to output

    hlt: "hlt",

    input: "input",
    output: "output",

    binary_raw: "binary",
    binary: { // ...c a b -> ...c (a oper b)
        "%": "mod",
        "+": "pls",
        "/": "frc",
        ">": "grt",
        "<": "lth",
        "==": "eql",
        "-": "mns",
        "||": "or", // выше гор
        "&&": "and"
    }
}

const string_reg = new RegExp(/(\bcharacter\b)\(\blen\b=(\d+)\) (\w+) : \"(.*)\"/)
const int_reg = new RegExp(/\binteger\b (\w+) : (.+)/)
const val_assign_post_init = new RegExp(/^(\w+) : (.+)$/)

const do_while_reg = new RegExp(/\bdo\b \bwhile\b\((.*)\)/)
const do_while_end_reg = new RegExp(/^end do$/)

const if_start_reg = new RegExp(/^if\((.*)\) then$/)
const else_reg = new RegExp(/^else$/)
const if_end_reg = new RegExp(/^endif$/)

const func_init_reg = new RegExp(/\bfunction\b (\w+)\(\)/)
const func_end_reg = new RegExp(/^end function$/)
const func_return_reg = new RegExp(/^\breturn\b$/)
const func_call_reg = new RegExp(/^\b(\w+)\b\(\)$/)

const output_reg = new RegExp(/^output\((\w+)\)$/)
const input_reg = new RegExp(/input\((\w+)\)/)

function load_operand_raw(name) {
    program_opers.push({
        opcode: opcode.push,
        param: var_name_m.get(name).mem_index,
        term: curr_file_line
    })
}

function condense_to_one(exprsArr) {
    let parts = exprsArr
    for (let i = 0; i < parts.length; i++) {
        let oper = parts[i].trim();
        if (isNaN(+oper)) {
            if (var_name_m.has(oper)) {
                program_opers.push({
                    opcode: opcode.load,
                    param: var_name_m.get(oper).mem_index,
                    term: curr_file_line
                })
            } else if (opcode.binary[oper]) {
                program_opers.push({
                    opcode: opcode.binary_raw,
                    param: opcode.binary[oper],
                    term: curr_file_line
                })
            } else throw "invalid term"
        } else {
            program_opers.push({
                opcode: opcode.push,
                param: oper,
                term: curr_file_line
            })
        }
    }
}

const add_pop_oper = () => {
    let pop_oper = {
        opcode: opcode.pop,
        param: null,
        term: curr_file_line
    }
    program_opers.push(pop_oper)
}

const var_type = {
    int: "integer",
    string: "character"
}

const block_parts = {
    do_while: "do while",
    function: "function",
    if: "if",
}

const port = {
    dev1: 0,
    dev2: 1
}

// save here the tasks for linking the addr
let block_stack = [] // stores operations that need to link address to jmp
// pushes the operation

let program_opers = []
let program_mem = []


function push_linking_job(operation, type_of_block) {
    block_stack.push({
        operation: operation,
        type_of_block: type_of_block
    })
}

// processes the operation
function process_linking_job(address_to_go, type_of_block) {
    if (!block_stack.length) throw "there is no block opening!"

    let last_opened_block = block_stack.pop()
    if (last_opened_block.type_of_block !== type_of_block) {
        throw `current open block is ${last_opened_block.type_of_block} and block being closed is ${type_of_block}!`
    }

    last_opened_block.operation.param = address_to_go
}

// current stuff
let curr_file_line = ""
let curr_func_name = ""

const preset_for_str_io = (str_mem_addr) => { // mem: 0 -> pointer, mem: 1 -> length
    program_opers.push({
        opcode: opcode.load,
        param: str_mem_addr,
        term: curr_file_line
    })
    program_opers.push({
        opcode: opcode.save,
        param: 1,
        term: curr_file_line
    })
    add_pop_oper()
    program_opers.push({
        opcode: opcode.push,
        param: str_mem_addr,
        term: curr_file_line
    })
    program_opers.push({
        opcode: opcode.push,
        param: 1,
        term: curr_file_line
    })
    program_opers.push({
        opcode: opcode.binary_raw,
        param: opcode.binary["+"],
        term: curr_file_line
    })
    program_opers.push({
        opcode: opcode.save,
        param: 0,
        term: curr_file_line
    })
    program_opers.push({
        opcode: opcode.pop,
        param: null,
        term: curr_file_line
    })
}

// func && var name maps
let func_info_m = new Map()
let var_name_m = new Map()

function init_helpers(){
    func_info_m.clear()
    var_name_m.clear()
    block_stack.length = 0
    func_info_m.set("input", {
        mem_index: 17
    })
    func_info_m.set("output", {
        mem_index: 1
    })

    program_mem.push(0)
    var_name_m.set("input_is_empty", {
        mem_index: 2
    })
}

// init hanlders
function init_int_var(name) {
    if (var_name_m.has(name)) throw "variable already defined!"
    var_name_m.set(name, {
        mem_index: program_mem.length,
        type: var_type.int
    })
    program_mem.push(0)

    program_opers.push({
        opcode: opcode.save,
        param: var_name_m.get(name).mem_index,
        term: curr_file_line
    })
}

function traverse_and_init_vars(file_content) {
    file_content = file_content.split("\n")
    for (let line_index = 0; line_index < file_content.length; line_index++) {
        curr_file_line = file_content[line_index].trim()
        if (curr_file_line === "") continue
        if (curr_file_line.split(" : ").length !== 2) continue

        if (int_reg.test(curr_file_line)) {
            let matches = Array.from(curr_file_line.matchAll(int_reg.source))[0]
            let name = matches[1]
            condense_to_one(matches[2].split(" "))
            init_int_var(name)
            add_pop_oper()
        } else if (string_reg.test(curr_file_line)) {
            let matches = Array.from(curr_file_line.matchAll(string_reg.source))[0]
            let [name, string_len, str_content] = [matches[3], matches[2], matches[4]]

            if (var_name_m.has(name)) throw "variable already defined!"
            var_name_m.set(name, {
                mem_index: program_mem.length,
                type: var_type.string
            })
            program_mem.push(string_len)
            let str_starts_at = var_name_m.get(name).mem_index + 1
            for (let i = 0; i < Math.max(+string_len, str_content.length); i++) {
                program_mem[str_starts_at + i] = (str_content[i] || 0)
            }
        }
    }
}


function translate(file_content) {
    file_content = file_content.split("\n")
    for (let line_index = 0; line_index < file_content.length; line_index++) {
        curr_file_line = file_content[line_index].trim()
        if (curr_file_line === "") continue

        if (val_assign_post_init.test(curr_file_line)) {
            let matches = Array.from(curr_file_line.matchAll(val_assign_post_init.source))[0]
            let var_info = var_name_m.get(matches[1])

            if (!var_info) throw "no such variable!"
            if (var_info.type === var_type.string) {
                throw "can not change vars of type string!"
            }
            condense_to_one(matches[2].split(" "))
            program_opers.push({
                opcode: opcode.save,
                param: var_info.mem_index,
                term: curr_file_line
            })
            add_pop_oper()
        } else if (if_start_reg.test(curr_file_line)) {
            let matches = Array.from(curr_file_line.matchAll(if_start_reg.source))[0]
            let expr = matches[1]
            condense_to_one(expr.split(" "))

            let oper_if_skip = {
                opcode: opcode.jnt,
                param: null,
                term: curr_file_line
            }
            program_opers.push(oper_if_skip)
            push_linking_job(oper_if_skip, block_parts.if)

        } else if (else_reg.test(curr_file_line)) {

            process_linking_job(program_opers.length+1, block_parts.if)

            let oper_else_skip = { // at the end of the if block (to skip else)
                opcode: opcode.jmp,
                param: null,
                term: curr_file_line
            }
            push_linking_job(oper_else_skip, block_parts.if)
            program_opers.push(oper_else_skip)

            let oper_to_jmp_from_if = {
                opcode: opcode.none,
                param: null,
                term: curr_file_line
            }
            program_opers.push(oper_to_jmp_from_if)

        } else if (if_end_reg.test(curr_file_line)) {

            let oper_to_jmp_from_if_end = {
                opcode: opcode.none,
                param: null,
                term: curr_file_line
            }
            process_linking_job(program_opers.length, block_parts.if)
            program_opers.push(oper_to_jmp_from_if_end)
        }

        else if (do_while_reg.test(curr_file_line)) {
            let matches = Array.from(curr_file_line.matchAll(do_while_reg.source))[0]
            let expr = matches[1]

            let oper_before_while_eval = {
                opcode: opcode.none,
                param: program_opers.length,
                term: curr_file_line
            }
            push_linking_job(oper_before_while_eval, block_parts.do_while)
            program_opers.push(oper_before_while_eval)

            condense_to_one(expr.split(" "))

            let jnt_before_while = {
                opcode: opcode.jnt,
                param: null,
                term: curr_file_line
            }

            program_opers.push(jnt_before_while)
            push_linking_job(jnt_before_while, block_parts.do_while)

        } else if (do_while_end_reg.test(curr_file_line)) {
            process_linking_job(program_opers.length + 1, block_parts.do_while)
            let jnt_at_while_end = {
                opcode: opcode.jmp,
                param: block_stack[block_stack.length - 1].operation.param,
                term: curr_file_line
            }
            process_linking_job(jnt_at_while_end.param, block_parts.do_while)

            program_opers.push(jnt_at_while_end)

            let oper_after_while = {
                opcode: opcode.none,
                param: program_opers.length,
                term: curr_file_line
            }
            program_opers.push(oper_after_while)
        } else if (func_init_reg.test(curr_file_line)) {
            if (curr_func_name !== "") throw "nested functions are not allowed!"

            // add the jmp to miss the function when not calling it
            let oper_before_func = {
                opcode: opcode.jmp,
                param: null, // intentionally left null, because linker later will fill it
                term: curr_file_line
            }
            program_opers.push(oper_before_func)
            // add to linker stack
            push_linking_job(oper_before_func, block_parts.function)

            let matches = Array.from(curr_file_line.matchAll(func_init_reg.source))[0]

            let func_name = matches[1]
            func_info_m.set(func_name, {
                name: func_name,
                mem_index: program_opers.length
            })
            program_opers.push({
                opcode: opcode.none,
                param: null,
                term: ""
            })

            curr_func_name = func_name

        } else if (func_return_reg.test(curr_file_line)) {

            if (curr_func_name === "") throw "return call outside function scope!"

            program_opers.push({
                opcode: opcode.ret,
                param: null,
                term: curr_file_line
            })

        } else if (func_end_reg.test(curr_file_line)) {
            let oper_after_func = {
                opcode: opcode.none,
                param: null,
                term: curr_file_line
            }
            process_linking_job(program_opers.length, block_parts.function)
            program_opers.push(oper_after_func)

            curr_func_name = ""
        } else if (func_call_reg.test(curr_file_line)) {

            let matches = Array.from(curr_file_line.matchAll(func_call_reg.source))[0]
            let func_name = matches[1]
            let func_info = func_info_m.get(func_name)

            if (!func_info) throw "no function with name " + func_name

            let oper_call_to_func = {
                opcode: opcode.call,
                param: func_info.mem_index,
                term: curr_file_line
            }
            program_opers.push(oper_call_to_func)

        }

        if (output_reg.test(curr_file_line)) {
            let matches = Array.from(curr_file_line.matchAll(output_reg.source))[0]
            let var_info = var_name_m.get(matches[1])
            if (var_info.type === var_type.string) {
                preset_for_str_io(var_info.mem_index)
                let output_func = func_info_m.get("output")
                program_opers.push({
                    opcode: opcode.call,
                    param: output_func.mem_index,
                    term: curr_file_line
                })
            } else {
                program_opers.push({
                    opcode: opcode.push,
                    param: var_info.mem_index,
                    term: curr_file_line
                })
                program_opers.push({
                    opcode: opcode.output,
                    param: port.dev1,
                    term: curr_file_line
                })
                add_pop_oper()
            }

        } else if (input_reg.test(curr_file_line)) {
            let matches = Array.from(curr_file_line.matchAll(input_reg.source))[0]
            let var_info = var_name_m.get(matches[1])
            if (var_info.type === var_type.string) {
                program_opers.push({
                    opcode: opcode.push,
                    param: false,
                    term: curr_file_line
                })
                program_opers.push({
                    opcode: opcode.save,
                    param: 2,
                    term: curr_file_line
                })
                add_pop_oper()
                preset_for_str_io(var_info.mem_index)
                let input_func = func_info_m.get("input")
                program_opers.push({
                    opcode: opcode.call,
                    param: input_func.mem_index,
                    term: curr_file_line
                })
            } else {
                program_opers.push({
                    opcode: opcode.push,
                    param: var_info.mem_index,
                    term: curr_file_line
                })
                program_opers.push({
                    opcode: opcode.input,
                    param: port.dev1,
                    term: curr_file_line
                })
                add_pop_oper()
            }
        }
    }
}

function pack_and_safe(source_f_name) {
    program_opers.push({
        opcode: opcode.hlt,
        param: null,
        term: curr_file_line
    })
    fs.writeFile("./output/" + source_f_name.split(".")[0] + "_output_instr.json", JSON.stringify(program_opers), () => {
    })
    fs.writeFile("./output/" + source_f_name.split(".")[0] + "_output_mem.json", JSON.stringify(program_mem), () => {
    })
}

import * as fs from "fs";


export function compile(input_file_name, program_content, instr_arr) {
    program_opers = instr_arr
    program_mem = [0, 0]

    init_helpers()
    traverse_and_init_vars(program_content)
    translate(program_content);

    if(program_mem.length > 1000) throw "too much memory used!"
    program_mem = program_mem.concat(new Array(1000 - program_mem.length).fill(0))
    pack_and_safe(input_file_name)

    return program_mem
}